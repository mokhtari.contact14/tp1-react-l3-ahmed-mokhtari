import React from "react";
import "./App.css";
import Topbar from "./Composents/Topbar.js";
import CD from "./Composents/CD.js";
import Footer from "./Composents/FT.js";

import axios from "axios";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {  Dk: [], champ: [], isValide: false };
    this.clk = this.clk.bind(this);
    this.clkisV = this.clkisV.bind(this);
  }

  componentDidMount() {
    axios
      .get( "https://los.ling.fr/cards" )
      .then(res => { this.setState({ champ: res.data });});
  }
  
  clkisV() {
    this.setState({ isValide: true });
  }

  clk(key) {
    const nwchamp = [...this.state.champ];
    const nwDeck = [...this.state.Dk];
    let IdChamp = nwchamp.findIndex(nb => {
      return nb.key === key;
    });
    if (IdChamp !== -1) {
      nwDeck.push(nwchamp[IdChamp]); 
      nwchamp.splice(IdChamp, 1); 
    }
    this.setState({
      champ: nwchamp,
      Dk: nwDeck
    });
  }
  render() {
    const {champ, Dk, isValide } = this.state;
    const DKF = Dk.length === this.props.nbCards;
    const cards = champ.map(champion => {
      return (
        <CD
          key={champion.key}
          champion={champion}
          click={!DKF ? this.clk : () => {}}
        />
      );
    });
    const IFDK = Dk.map(champion => {
      return (
        <CD
          key={champion.key}
          champion={champion}
          click={!isValide ? this.clk : () => {}}
        />
      );
    });
    const validateButton = (
      <button
        className="float-right"
        onClick={this.clkisV}
      >
        J'ai fini!
      </button>
    );
    const ValBut = DKF ? validateButton : null;

    const Inter = (
      <main className="container-fluid flex-fill d-flex  justify-content-center p-0">
        <div className="container-fluid flex-fill py-2 d-flex flex-column justify-content-center text-light">
          <h1>Champions Disponibles</h1>
          <div className="row justify-content-center pb-2">{cards}</div>
        </div>
        <div className="container-fluid flex-fill py-2 d-flex flex-column border-left border-1 border-dark bg-dark text-light">
          <h1>Mon Deck {ValBut}</h1>
          <div className="row justify-content-center pb-2"> {IFDK} </div>
        </div>
      </main>
    );

    const ETF = (
      <main className="container-fluid flex-fill d-flex  justify-content-center p-0">
        <div className="container-fluid flex-fill py-2 d-flex flex-column border-left border-1 border-dark bg-dark text-light">
          <h1>Le Deck a été validé!</h1>
        </div>
      </main>
    );
    return (
      <div>
        <Topbar />
        <div className="main__div">
        {isValide ? ETF : Inter}
        </div>
        <Footer />
      </div>
    );
  }
}
export default App;

