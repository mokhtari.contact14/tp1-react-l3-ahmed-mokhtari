import React from "react";

class CD extends React.Component {
  
    render() {
        const {champion, click} = this.props;
        const img ="https://ddragon.leagueoflegends.com/cdn/img/champion/splash/" + champion.key + "_0.jpg";
        return (
            <div 
                className="col-12 col-sm-3 CD"
                onClick={click.bind(this, champion.key)}
            >
                <article className="info text-white">
                    <div className="bg-dark ">
                        <img src={img} alt="champion" width="100%" height="auto" />
                        <div >
                            Attaque : {champion.info.attack}<br />
                            Armure : {champion.info.defense}
                        </div>
                    </div> 
                    <p className="info text-white">{champion.name}</p>
                </article>
            </div>
        );
    }
}
export default CD;