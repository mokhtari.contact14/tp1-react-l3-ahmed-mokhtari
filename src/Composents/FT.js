import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <footer className="FT">
        <p>Copyright L3-MIASHS Informatique - 2020-2021</p>
      </footer>
    );
  }
}
export default Footer;
