import React from "react";

class Topbar extends React.Component {
  render() {
    return (
      <header>
        <div className="top bg-dark">
          <h1>League Of StoneS</h1>
        </div>
      </header>
    );
  }
}
export default Topbar;